const { TasksModel } = require('./tasks');
const { CommentsModel } = require('./comments');
const { StatusesAndPrioritiesModel } = require('./statuses_priorities');

const { Statuses, Priorities } = new StatusesAndPrioritiesModel();
const { Comments } = new CommentsModel();
const { Tasks } = new TasksModel();

module.exports = {
  Statuses,
  Priorities,
  Comments,
  Tasks,
};

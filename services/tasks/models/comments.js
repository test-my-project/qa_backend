const { AbstractDB } = require('./DB');

class CommentsModel extends AbstractDB {
  constructor () {
    super();
    this.comments = this.sequelize.define('comments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: this.DataTypes.INTEGER,
      },
      text: {
        allowNull: false,
        type: this.DataTypes.STRING,
      },
      user_id: {
        allowNull: false,
        type: this.DataTypes.INTEGER,
      },
      task_id: {
        type: this.DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: 'tasks',
          key: 'id',
        },
        onUpdate: 'cascade',
        onDelete: 'cascade',
      },
      coordinates_x: {
        allowNull: true,
        type: this.DataTypes.INTEGER,
      },
      coordinates_y: {
        allowNull: true,
        type: this.DataTypes.INTEGER,
      },
      window_width: {
        allowNull: true,
        type: this.DataTypes.INTEGER,
        comment: 'pixels',
      },
      window_height: {
        allowNull: true,
        type: this.DataTypes.INTEGER,
        comment: 'pixels',
      },
    }, {
      underscored: true,
      tableName: 'comments',
    });
  }

  get Comments () {
    return this.comments;
  }
}

module.exports = { CommentsModel };
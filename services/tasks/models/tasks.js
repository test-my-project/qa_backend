const crypto = require('crypto');

const { AbstractDB } = require('./DB');
const { CommentsModel } = require('./comments');

const { Comments } = new CommentsModel();

class TasksModel extends AbstractDB {
  constructor () {
    super();
    this.tasks = this.sequelize.define('tasks', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: this.DataTypes.INTEGER,
      },
      project_id: {
        allowNull: false,
        type: this.DataTypes.INTEGER,
      },
      user_id: {
        allowNull: false,
        type: this.DataTypes.INTEGER,
      },
      assignee_user_id: {
        allowNull: true,
        type: this.DataTypes.INTEGER,
      },
      title: {
        allowNull: false,
        type: this.DataTypes.STRING,
      },
      description: {
        type: this.DataTypes.STRING,
      },
      task_status_id: {
        type: this.DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: 'tasks_status_dictionary',
          key: 'id',
        },
        onUpdate: 'cascade',
        onDelete: 'cascade',
      },
      task_priority_id: {
        type: this.DataTypes.INTEGER,
        allowNull: true,
        references: {
          model: 'tasks_priority_dictionary',
          key: 'id',
        },
        onUpdate: 'cascade',
        onDelete: 'cascade',
      },
      window_width: {
        allowNull: false,
        type: this.DataTypes.INTEGER,
        comment: 'pixels',
      },
      window_height: {
        allowNull: false,
        type: this.DataTypes.INTEGER,
        comment: 'pixels',
      },
      url: {
        allowNull: false,
        type: this.DataTypes.STRING,
      },
      coordinates_x: {
        allowNull: false,
        type: this.DataTypes.INTEGER,
      },
      coordinates_y: {
        allowNull: false,
        type: this.DataTypes.INTEGER,
      },
      work_time: {
        allowNull: true,
        type: this.DataTypes.INTEGER,
        comment: 'in minutes',
      },
      link_hash: {
        type: this.DataTypes.STRING('32'),
        allowNull: false,
        defaultValue: () => crypto.createHash('md5').
          update(new Date().valueOf().toString()).
          digest('hex'),
      },
      link_is_active: {
        allowNull: false,
        type: this.DataTypes.BOOLEAN,
        defaultValue: false,
      },
      link_is_commentable: {
        allowNull: false,
        type: this.DataTypes.BOOLEAN,
        defaultValue: false,
      },
    }, {
      underscored: true,
      tableName: 'tasks',
    });

    this.tasks.hasMany(Comments, { foreignKey: 'task_id' });
  }

  get Tasks () {
    return this.tasks;
  }
}

module.exports = { TasksModel };
const { AbstractDB } = require('./DB');

class StatusesAndPrioritiesModel extends AbstractDB {
  constructor () {
    super();
    this.statuses = this.sequelize.define('tasks_status_dictionary', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: this.DataTypes.INTEGER,
      },
      title: {
        allowNull: false,
        type: this.DataTypes.STRING,
      },
    }, {
      underscored: true,
      tableName: 'tasks_status_dictionary',
    });
    this.priorities = this.sequelize.define('tasks_priority_dictionary', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: this.DataTypes.INTEGER,
      },
      title: {
        allowNull: false,
        type: this.DataTypes.STRING,
      },
    }, {
      underscored: true,
      tableName: 'tasks_priority_dictionary',
    });
  }

  get Statuses () {
    return this.statuses;
  }

  get Priorities () {
    return this.priorities;
  }
}

module.exports = { StatusesAndPrioritiesModel };
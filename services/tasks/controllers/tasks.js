const { Router } = require('restify-router');

const { Comments, Tasks } = require('../models');
const { AuthMiddleware } = require('./authMiddleware');

const routerInstance = new Router();

class Task {
  /**
   * @swagger
   * definitions:
   *   Task:
   *     type: object
   *     properties:
   *       id:
   *         type: integer
   *       project_id:
   *         type: integer
   *       assignee_user_id:
   *         type: integer
   *       title:
   *         type: string
   *       description:
   *         type: string
   *       window_width:
   *         type: integer
   *       window_height:
   *         type: integer
   *       url:
   *         type: string
   *       coordinates_x:
   *         type: integer
   *       coordinates_y:
   *         type: integer
   *       work_time:
   *         type: integer
   *       task_status_id:
   *         type: integer
   *       task_priority_id:
   *         type: integer
   *       link_hash:
   *         type: string
   *       link_is_active:
   *         type: boolean
   *       link_is_commentable:
   *         type: boolean
   *       comments:
   *         type: array
   *         items:
   *          $ref: '#/definitions/Comment'
   *
   *   CreateTask:
   *     type: object
   *     properties:
   *       project_id:
   *         type: integer
   *       assignee_user_id:
   *         type: integer
   *       title:
   *         type: string
   *       description:
   *         type: string
   *       window_width:
   *         type: integer
   *       window_height:
   *         type: integer
   *       url:
   *         type: string
   *       coordinates_x:
   *         type: integer
   *       coordinates_y:
   *         type: integer
   *       work_time:
   *         type: integer
   *       task_status_id:
   *         type: integer
   *       task_priority_id:
   *         type: integer
   *
   *   UpdateTaskShareLink:
   *     type: object
   *     properties:
   *       link_is_active:
   *         type: boolean
   *       link_is_commentable:
   *         type: boolean
   *
   * */

  constructor (server) {
    this.registerRoutes();
    return routerInstance.applyRoutes(server);
  }

  registerRoutes () {
    routerInstance.get('/tasks', AuthMiddleware.checkApplicationToken, Task.getAllTasks.bind(this));
    routerInstance.get('/tasks/:task_id', AuthMiddleware.checkApplicationToken,
      Task.getAllTasks.bind(this));
    routerInstance.put('/tasks/:task_id', AuthMiddleware.checkApplicationToken,
      Task.updateTaskInfo.bind(this));
    routerInstance.post('/tasks', AuthMiddleware.checkApplicationToken, Task.createTask.bind(this));
    routerInstance.get('/tasks/share/:link_hash', AuthMiddleware.checkApplicationToken,
      Task.getAllTasks.bind(this));
    routerInstance.put('/tasks/:task_id/share', AuthMiddleware.checkApplicationToken,
      Task.updateTaskShareLink.bind(this));
  }

  static async getAllTasks (req, res, next) {

    /**
     * @swagger
     * /tasks:
     *   get:
     *     tags:
     *      - Tasks
     *     description: Getting all tasks
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: project_id
     *         description: Project id.
     *         in: query
     *         required: false
     *         type: string
     *       - name: user_id
     *         description: User id.
     *         in: query
     *         required: false
     *         type: string
     *       - name: assignee_user_id
     *         description: User id witch assigned tasks.
     *         in: query
     *         required: false
     *         type: string
     *       - name: get_comments
     *         description: include to select comments by task_id
     *         in: query
     *         type: boolean
     *     responses:
     *       200:
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/Task'
     *
     * /tasks/{task_id}:
     *   get:
     *     tags:
     *      - Tasks
     *     description: Getting task by task id
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: task_id
     *         description: Task id
     *         in: path
     *         required: true
     *         type: string
     *       - name: get_comments
     *         description: include to select comments by task_id
     *         in: query
     *         type: boolean
     *     responses:
     *       200:
     *         schema:
     *           $ref: '#/definitions/Task'
     * /tasks/share/{link_hash}:
     *   get:
     *     tags:
     *      - Tasks
     *     description: Getting task by Hash from share url
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: link_hash
     *         description: Hash from share url
     *         in: path
     *         required: true
     *         type: string
     *       - name: get_comments
     *         description: include to select comments by task_id
     *         in: query
     *         type: boolean
     *     responses:
     *       200:
     *         schema:
     *           $ref: '#/definitions/Task'
     */
    const { project_id, user_id, get_comments, assignee_user_id } = req.query;
    const query = { where: {} };

    if (project_id && !!project_id.trim()) { query.where.project_id = project_id; }
    if (user_id && !!user_id.trim()) { query.where.user_id = user_id; }
    if (assignee_user_id &&
      !!assignee_user_id.trim()) { query.where.assignee_user_id = assignee_user_id; }

    if (get_comments === 'true') {
      query.include = [
        {
          model: Comments,
        },
      ];
    }

    if (req.params.task_id && !!req.params.task_id.trim()) {
      query.where.id = req.params.task_id;
    }

    if (req.params.link_hash && /\b[a-f0-9]{32}\b/) {
      query.where = { link_hash: req.params.link_hash, link_is_active: true };
    }

    try {
      const result = await Tasks.findAll(query);
      if (
        req.params.task_id && !!req.params.task_id.trim() ||
        req.params.link_hash && /\b[a-f0-9]{32}\b/
      ) {
        return res.send(result[0]);
      }
      return res.send(result);
    } catch (e) {
      console.error(e);
      return res.send(500, { error: '' });
    }
  }

  static async createTask (req, res, next) {

    /**
     * @swagger
     * /tasks:
     *   post:
     *     tags:
     *      - Tasks
     *     description: Create new task
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: access-token
     *         description: User access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: body
     *         in: body
     *         type: object
     *         schema:
     *           $ref: '#/definitions/CreateTask'
     *     responses:
     *       200:
     *         schema:
     *           $ref: '#/definitions/Task'
     */
    let { body } = req;

    try {
      const { payload: { id } } = await AuthMiddleware.decodeUserJWT(req.headers['access-token']);
      body.user_id = id;
    } catch (e) {
      return res.send(401, e);
    }
    try {
      return res.send(await Tasks.create(body));
    } catch (e) {
      return res.send(e.statusCode, e);
    }
  }

  static async updateTaskInfo (req, res, next) {

    /**
     * @swagger
     * /tasks/{task_id}:
     *   put:
     *     tags:
     *      - Tasks
     *     description: update task
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: access-token
     *         description: User access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: task_id
     *         description: Task id.
     *         in: path
     *         required: true
     *         type: string
     *       - name: body
     *         in: body
     *         type: object
     *         schema:
     *           $ref: '#/definitions/CreateTask'
     *     responses:
     *       200:
     *         schema:
     *           $ref: '#/definitions/Task'
     */
    const { task_id } = req.params;
    const { body } = req;

    if (!task_id) {
      return res.send(400, {
        message: 'Task id empty',
      });
    }
    try {
      const { payload: { id } } = await AuthMiddleware.decodeUserJWT(req.headers['access-token']);
      body.user_id = id;
    } catch (e) {
      return res.send(401, e);
    }
    try {
      await Tasks.update(body, { where: { id: task_id } });
      return res.send(await Tasks.findByPk(task_id));
    } catch (e) {
      return res.send(e.statusCode, e);
    }
  }

  static async updateTaskShareLink (req, res, next) {

    /**
     * @swagger
     * /tasks/{task_id}/share:
     *   put:
     *     tags:
     *      - Tasks
     *     description: enable/disable share link & rules
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: access-token
     *         description: User access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: task_id
     *         description: Task id.
     *         in: path
     *         required: true
     *         type: string
     *       - name: body
     *         in: body
     *         type: object
     *         schema:
     *           $ref: '#/definitions/UpdateTaskShareLink'
     *     responses:
     *       200:
     *         schema:
     *           $ref: '#/definitions/Task'
     */
    let { task_id } = req.params,
      { body: { link_is_active, link_is_commentable } } = req;
    if (!task_id) {
      return res.send(400, {
        message: 'Task id empty',
      });
    }
    try {
      await AuthMiddleware.decodeUserJWT(req.headers['access-token']);
    } catch (e) {
      return res.send(401, e);
    }
    try {
      await Tasks.update({ link_is_active, link_is_commentable }, { where: { id: task_id } });
      return res.send(await Tasks.findByPk(task_id));
    } catch (e) {
      return res.send(e.statusCode, e);
    }
  }
}

module.exports = { Task };

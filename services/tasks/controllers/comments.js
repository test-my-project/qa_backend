const { Router } = require('restify-router');

const { AuthMiddleware } = require('./authMiddleware');
const { Comments } = require('../models');

const routerInstance = new Router();

class Comment {
  constructor (server) {
    this.registerRoutes();
    return routerInstance.applyRoutes(server);
  }

  registerRoutes () {
    routerInstance.get('/tasks/:task_id/comments', AuthMiddleware.checkApplicationToken,
      this.getAllComments.bind(this));
    routerInstance.get('/tasks/:task_id/comments/:comment_id', AuthMiddleware.checkApplicationToken,
      this.getAllComments.bind(this));
    routerInstance.put('/tasks/:task_id/comments/:comment_id', AuthMiddleware.checkApplicationToken,
      this.updateCommentInfo.bind(this));
    routerInstance.post('/tasks/:task_id/comments', AuthMiddleware.checkApplicationToken,
      this.createComment.bind(this));
  }

  /**
   * @swagger
   * definitions:
   *   Comment:
   *     type: object
   *     properties:
   *       id:
   *         type: integer
   *       text:
   *         type: string
   *       user_id:
   *         type: integer
   *       task_id:
   *         type: integer
   *       coordinates_x:
   *         type: integer
   *       coordinates_y:
   *         type: integer
   *       window_width:
   *         type: integer
   *       window_height:
   *         type: integer
   *       email:
   *         type: string
   *   CreateComment:
   *     type: object
   *     properties:
   *       text:
   *         type: string
   *       coordinates_x:
   *         type: integer
   *       coordinates_y:
   *         type: integer
   *       window_width:
   *         type: integer
   *       window_height:
   *         type: integer
   *       email:
   *         type: string
   * */

  async getAllComments (req, res, next) {

    /**
     * @swagger
     * /tasks/{task_id}/comments:
     *   get:
     *     tags:
     *      - Comments
     *     description: Getting all comments
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: task_id
     *         description: User id.
     *         in: path
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/Comment'
     *
     * /tasks/{task_id}/comments/{comment_id}:
     *   get:
     *     tags:
     *      - Comments
     *     description: Getting comment by comment id
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: task_id
     *         description: User id.
     *         in: path
     *         required: true
     *         type: string
     *       - name: comment_id
     *         description: User id.
     *         in: path
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/Comment'
     */
    const { task_id, comment_id } = req.params;
    if (!task_id) {
      return res.send(400, {
        message: 'Task id empty',
      });
    }
    let query = { task_id };
    if (comment_id) { query.comment_id = comment_id; }
    try {
      return res.send(await Comments.findAll({
        where: query,
      }));
    } catch (e) {
      return res.send(500, { error: '' });
    }
  }

  async updateCommentInfo (req, res, next) {

    /**
     * @swagger
     * /tasks/{task_id}/comments/{comment_id}:
     *   put:
     *     tags:
     *      - Comments
     *     description: Getting info by user access_token
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: comment_id
     *         description: User id.
     *         in: path
     *         required: true
     *         type: string
     *       - name: task_id
     *         description: User id.
     *         in: path
     *         required: true
     *         type: string
     *       - name: body
     *         in: body
     *         type: object
     *         schema:
     *           $ref: '#/definitions/CreateComment'
     *     responses:
     *       200:
     *         schema:
     *           $ref: '#/definitions/Comment'
     */

    const { task_id, comment_id } = req.params;
    const { body } = req;

    if (!task_id) {
      return res.send(400, {
        message: 'Task id empty',
      });
    }
    if (!comment_id) {
      return res.send(400, {
        message: 'Comment id empty',
      });
    }
    try {
      const { payload: { id } } = await AuthMiddleware.decodeUserJWT(req.headers['access-token']);
      body.user_id = id;
    } catch (e) {
      return res.send(401, e);
    }
    delete body.task_id;
    try {
      await Comments.update(body, { where: { id: comment_id, task_id } });
      return res.send(await Comments.findByPk(comment_id));
    } catch (e) {
      return res.send(e.statusCode, e);
    }
  }

  async createComment (req, res, next) {

    /**
     * @swagger
     * /tasks/{task_id}/comments:
     *   post:
     *     tags:
     *      - Comments
     *     description: Getting info by user access_token
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: access-token
     *         description: User access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: task_id
     *         description: User id.
     *         in: path
     *         required: true
     *         type: string
     *       - name: body
     *         in: body
     *         type: object
     *         schema:
     *           $ref: '#/definitions/CreateComment'
     *     responses:
     *       200:
     *         schema:
     *           $ref: '#/definitions/Comment'
     */

    const { task_id } = req.params;
    const { body } = req;

    if (!task_id) {
      return res.send(400, { message: 'Comment id empty' });
    }
    body.task_id = task_id;

    try {
      const { payload: { id } } = await AuthMiddleware.decodeUserJWT(req.headers['access-token']);
      body.user_id = id;
    } catch (e) {
      return res.send(401, e);
    }
    if (req.headers['access-token']) {
      try {
        const { payload: { id } } = await AuthMiddleware.decodeUserJWT(req.headers['access-token']);
        body.user_id = id;
      } catch (e) {
        return res.send(401, e);
      }
    } else if (!body.email) {
      return res.send(401, { message: 'Access token or email is empty' });
    }

    try {
      res.send(await Comments.create(body));
    } catch (e) {
      return res.send(e.statusCode, e);
    }
  }
}

module.exports = { Comment };

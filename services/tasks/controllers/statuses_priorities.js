const { Router } = require('restify-router');

const { Statuses, Priorities } = require('../models');
const { AuthMiddleware } = require('./authMiddleware');
const routerInstance = new Router();

class StatusAndPriorities {
  constructor (server) {
    this.registerRoutes();
    return routerInstance.applyRoutes(server);
  }

  registerRoutes () {
    routerInstance.get('/statuses', AuthMiddleware.checkApplicationToken,
      this.getAllStatuses.bind(this));
    routerInstance.get('/priorities', AuthMiddleware.checkApplicationToken,
      this.getAllPriorities.bind(this));
  }

  async getAllStatuses (req, res, next) {

    /**
     * @swagger
     * /statuses:
     *   get:
     *     tags:
     *      - Tasks
     *     description: Getting all statuses
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/UserSubscribe'
     *
     */

    try {
      return res.send(await Statuses.findAll());
    } catch (e) {
      return res.send(500, { error: '' });
    }
  }

  async getAllPriorities (req, res, next) {

    /**
     * @swagger
     * /priorities:
     *   get:
     *     tags:
     *      - Tasks
     *     description: Getting all priorities
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/UserSubscribe'
     *
     */

    try {
      return res.send(await Priorities.findAll());
    } catch (e) {
      return res.send(500, { error: '' });
    }
  }

}

module.exports = { StatusAndPriorities };

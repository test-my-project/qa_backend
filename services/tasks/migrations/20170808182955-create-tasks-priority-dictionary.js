'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return queryInterface.createTable('tasks_priority_dictionary', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        allowNull: false,
        type: Sequelize.STRING
      },
      created_at: {
        type: Sequelize.DATE,
        defaultValue:Sequelize.literal('now()'),
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        defaultValue:Sequelize.literal('now()'),
        allowNull: false
      }
    }, {
      uniqueKeys: [
        {
          name: 'tasks_priority_dictionary_unique',
          singleField: false,
          fields: ['title']
        },
      ]
    }).then(function () {
      queryInterface.bulkInsert('tasks_priority_dictionary', [
        {
          //kyiv-notify
          title: 'low'
        },
        {
          //kyiv-accident
          title: 'normal'
        },
        {
          //kyiv-osvita
          title: 'high'
        },
        {
          //kyiv-id
          title: 'blocker'
        }
      ]);
    });
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    return queryInterface.dropTable('tasks_priority_dictionary');
  }
};

'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => Promise.all([
    queryInterface.addColumn({ tableName: 'tasks' }, 'link_hash', {
      allowNull: false,
      type: Sequelize.STRING('32')
    }),
    queryInterface.addColumn({ tableName: 'tasks' }, 'link_is_active', {
      allowNull: false,
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    }),
    queryInterface.addColumn({ tableName: 'tasks' }, 'link_is_commentable', {
      allowNull: false,
      type: Sequelize.BOOLEAN,
      defaultValue: false
    }),
    queryInterface.addIndex('tasks', ['link_hash'], {
      type: 'unique',
      name: 'tasks_link_hash_uindex',
    }),
  ]),

  down: (queryInterface, Sequelize) => Promise.all([
    queryInterface.removeColumn({ tableName: 'tasks' }, 'link_hash'),
    queryInterface.removeColumn({ tableName: 'tasks' }, 'link_is_active'),
    queryInterface.removeColumn({ tableName: 'tasks' }, 'link_is_commentable'),
    queryInterface.removeIndex('tasks', 'link_hash'),
  ])
};

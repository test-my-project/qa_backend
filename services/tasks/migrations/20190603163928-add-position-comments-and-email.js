'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => Promise.all([
    queryInterface.addColumn({ tableName: 'comments' }, 'coordinates_x', {
      allowNull: true,
      type: Sequelize.INTEGER
    }),
    queryInterface.addColumn({ tableName: 'comments' }, 'coordinates_y', {
      allowNull: true,
      type: Sequelize.INTEGER
    }),
    queryInterface.addColumn({ tableName: 'comments' }, 'window_width', {
      allowNull: true,
      type: Sequelize.INTEGER
    }),
    queryInterface.addColumn({ tableName: 'comments' }, 'window_height', {
      allowNull: true,
      type: Sequelize.INTEGER
    }),
    queryInterface.addColumn({ tableName: 'comments' }, 'email', {
      allowNull: true,
      type: Sequelize.STRING
    }),
    queryInterface.removeColumn({ tableName: 'comments' }, 'user_id').then(() =>
      queryInterface.addColumn({ tableName: 'comments' }, 'user_id', {
        allowNull: true,
        type: Sequelize.INTEGER,
      })
    ),
  ]),

  down: (queryInterface, Sequelize) => Promise.all([
    queryInterface.removeColumn({ tableName: 'comments' }, 'coordinates_x'),
    queryInterface.removeColumn({ tableName: 'comments' }, 'coordinates_y'),
    queryInterface.removeColumn({ tableName: 'comments' }, 'window_width'),
    queryInterface.removeColumn({ tableName: 'comments' }, 'window_height'),
    queryInterface.removeColumn({ tableName: 'comments' }, 'email'),
    queryInterface.changeColumn({ tableName: 'comments' }, 'user_id', {
      allowNull: false,
      type: Sequelize.STRING,
    }),
  ])
};

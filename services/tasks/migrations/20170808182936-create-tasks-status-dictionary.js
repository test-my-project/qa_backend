'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
       Add altering commands here.
       Return a promise to correctly handle asynchronicity.

       Example:
       return queryInterface.createTable('users', { id: Sequelize.INTEGER });
       */
    return queryInterface.createTable('tasks_status_dictionary', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      title: {
        allowNull: false,
        type: Sequelize.STRING
      },
      created_at: {
        type: Sequelize.DATE,
        defaultValue:Sequelize.literal('now()'),
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        defaultValue:Sequelize.literal('now()'),
        allowNull: false
      }
    }, {
      uniqueKeys: [
        {
          name: 'tasks_status_dictionary_unique',
          singleField: false,
          fields: ['title']
        },
      ]
    }).then(function () {
      queryInterface.bulkInsert('tasks_status_dictionary', [
        {
          title: 'New'
        },
        {
          title: 'In progress'
        },
        {
          title: 'Done'
        },
        {
          title: 'Closed'
        },
        {
          title: 'Reopen'
        },
        {
          title: 'Test'
        },
        {
          title: 'On hold'
        }
      ]);
    });
  },

  down: function (queryInterface, Sequelize) {
    /*
       Add reverting commands here.
       Return a promise to correctly handle asynchronicity.

       Example:
       return queryInterface.dropTable('users');
       */
    return queryInterface.dropTable('tasks_status_dictionary');
  }
};

'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return queryInterface.createTable('tasks', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      project_id: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      user_id: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      title: {
        allowNull: false,
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.STRING
      },
      task_status_id:{
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'tasks_status_dictionary',
          key: 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },
      task_priority_id:{
        type: Sequelize.INTEGER,
        allowNull: true,
        references: {
          model: 'tasks_priority_dictionary',
          key: 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },
      window_width: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      window_height: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      url: {
        allowNull: false,
        type: Sequelize.STRING
      },
      coordinates_x: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      coordinates_y: {
        allowNull: false,
        type: Sequelize.INTEGER
      },
      work_time: {
        allowNull: true,
        type: Sequelize.INTEGER,
        comment:'in minutes'
      },
      created_at: {
        type: Sequelize.DATE,
        defaultValue:Sequelize.literal('now()'),
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        defaultValue:Sequelize.literal('now()'),
        allowNull: false
      }
    });
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    return queryInterface.dropTable('tasks');
  }
};

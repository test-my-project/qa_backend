# tasks

## Scripts

`yarn start` - start tasks service
`yarn migrations-up` - run migrations
`yarn migrations-down` - rollback last migration
`yarn migrations-create test` - creates a new migration with name `test`

## Api

* [tasks swagger](https://comments.qa-services.gq/swagger/)



const restify = require ('restify');
const plugins = require ('restify-plugins');
const swaggerJSDoc = require ('swagger-jsdoc');
const swaggerUi = require ('swagger-ui-restify');
const CookieParser = require ('restify-cookies');
const config = require ('config');

const { Template } = require ('./controllers/temlates');
const { Mail } = require ('./controllers/mail');

const server = restify.createServer ({
  name: 'qa-comments',
  version: '0.0.1',
});

server.pre (restify.pre.sanitizePath ());
server.use (plugins.acceptParser (server.acceptable));
server.use (plugins.queryParser ());
server.use (plugins.bodyParser ());
server.use (CookieParser.parse);

new Template (server);
new Mail (server);

if (process.env.NODE_ENV === 'localhost') {
  server.pre (restify.CORS ());
  server.pre (restify.fullResponse ());

  let unknownMethodHandler = function(req, res) {
    if (req.method.toLowerCase () === 'options') {
      const allowHeaders = [
        'Accept',
        'Accept-Version',
        'Content-Type',
        'Api-Version',
        'Origin',
        'X-Requested-With',
        'Authorization']; // added Origin & X-Requested-With & **Authorization**

      if (res.methods.indexOf ('OPTIONS') === -1) res.methods.push ('OPTIONS');

      res.header ('Access-Control-Allow-Credentials', true);
      res.header ('Access-Control-Allow-Headers', allowHeaders.join (', '));
      res.header ('Access-Control-Allow-Methods', res.methods.join (', '));
      res.header ('Access-Control-Allow-Origin', req.headers.origin);

      return res.send (200);
    } else {
      return res.send (new restify.MethodNotAllowedError ());
    }
  };

  server.on ('MethodNotAllowed', unknownMethodHandler);
}

const options = {
  swaggerDefinition: {
    info: {
      title: 'API documentation for email-messenger service', // Page title (required)
      version: '0.0.1', // Server version (required)
    },
  },
  apis: [`${ __dirname }/controllers/*.js`, `${ __dirname }/server.js`], // Path to the API docs
  tags: [
    {
      name: 'Templates',
    },
    {
      name: 'Mail',
    },
  ],
};

const swaggerSpec = swaggerJSDoc (options);

server.get ('/swagger/swagger.json', function(req, res) {
  res.setHeader ('Content-Type', 'application/json');
  res.send (swaggerSpec);
});

server.get (/\/swagger-ui+.*/, ...swaggerUi.serveFiles (swaggerSpec));
server.get ('/swagger', swaggerUi.setup (swaggerSpec));

server.get (/.*/, restify.serveStatic ({
  directory: __dirname + '/coverage/lcov-report',
  default: 'index.html',
}));

server.listen (process.env.port || config.get('serverPort') || 3000, function() {
  console.log ('%s listening at %s', server.name, server.url);
});

module.exports = server;

/**
 * @swagger
 * definitions:
 *
 *   Mail:
 *     type: object
 *     properties:
 *       template:
 *         type: string
 *       params:
 *         type: any
 *       emails:
 *         type: array
 *         items:
 *          type: string
 */

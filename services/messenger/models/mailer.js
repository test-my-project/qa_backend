const config = require('config');
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const { EmailTemplate } = require('email-templates');

const { AbstractDB } = require('./DB');
const { checkParams } = require('./templates');

class Mails extends AbstractDB {
  constructor () {
    super();
    this.sentMails = this.sequelize.define('sent_mails', {
      id: {
        type: this.DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      template: { type: this.DataTypes.STRING },
      params: { type: this.DataTypes.JSON },
      createdAt: { type: this.DataTypes.DATE },
      updatedAt: { type: this.DataTypes.DATE },
    }, { tableName: 'sent_mails' });
  }
}

class Mailer extends Mails {
  constructor () {
    super();
    this.transporter = nodemailer.createTransport(smtpTransport({
      host: config.get('mailer.host'),
      port: 465,
      secure: true, // use TLS
      auth: config.get('mailer.auth'),
    }));
  }

  async sendMail (template, params) {
    const { path, config: paramsConfig } = await checkParams(template, params);
    const sendConfirm = this.transporter.templateSender(new EmailTemplate(path),
      {
        from: config.get('mailer.sender_email'),
      }, params);

    try {
      await sendConfirm({
        to: params.emails,
        subject: paramsConfig.subject,
      }, params);
      await this.sentMails.create({ template, params });
      return { success: true };
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  all () {
    return this.sentMails.findAll();
  }
}

module.exports = { Mailer };

const util = require('util');
const fs = require('fs');
const path = require('path');
const { difference } = require('lodash');

const readFile = util.promisify(fs.readFile);
const readDir = util.promisify(fs.readdir);
const tplPath = path.resolve(__dirname, '..', 'templates');

async function all (tpl) {
  const templates = {};

  let dir = await readDir(tplPath);
  if (tpl) {
    dir = dir.filter(d => d === tpl);
  }

  await Promise.all(dir.map(async template => {
    const files = await readDir(`${ tplPath }/${ template }`);

    templates[template] = {};
    await Promise.all(files.map(async fileName => {
      let file = await readFile(`${ tplPath }/${ template }/${ fileName }`,
        'utf8');

      if (fileName.endsWith('.json')) {
        file = JSON.parse(file);
      }

      templates[template][fileName] = file;
      return { [fileName]: file };
    }));
  }));

  return templates;
}

async function checkParams (template, properties = {}) {
  const config = JSON.parse(
    await readFile(`${ tplPath }/${ template }/config.json`, 'utf8'));
  const required = config.parameters.filter(el => el.required).
    map(el => el.name);
  const propsKeys = [];
  for (const [key, value] of Object.entries(properties)) {
    if (value && typeof value !== 'string' || typeof value === 'string' &&
      !!String(value).trim() === true) {
      propsKeys.push(key);
    }
  }
  const diff = difference(required, propsKeys);
  if (diff.length) {
    throw new Error(`Not all params is valid: ${ diff.map(
      v => `parameter ${ v } is required`).join(', ') }`);
  }
  return {
    path: `${ tplPath }/${ template }`,
    config,
  };
}

all();

module.exports = { all, checkParams };

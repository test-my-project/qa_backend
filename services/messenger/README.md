# messenger

## Scripts

`yarn start` - start messenger service
`yarn migrations-up` - run migrations
`yarn migrations-down` - rollback last migration
`yarn migrations-create test` - creates a new migration with name `test`

## Api

* [Mails](https://messenger.qa-services.gq/swagger#/)



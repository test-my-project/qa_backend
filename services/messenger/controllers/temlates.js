const { AuthMiddleware } = require('./authMiddleware');
const templateModel = require('../models/templates');

const { Router } = require('restify-router');
const routerInstance = new Router();

class Template {
  constructor(server) {
    this.registerRoutes();
    return routerInstance.applyRoutes(server);
  }

  registerRoutes() {
    routerInstance.get('/templates', AuthMiddleware.checkApplicationToken(),
      this.getAllTemplates.bind(this));
    routerInstance.get('/templates/:template_name',
      AuthMiddleware.checkApplicationToken(),
      this.getAllTemplates.bind(this));
  }

  async getAllTemplates(req, res, next) {

    /**
     * @swagger
     * /templates:
     *   get:
     *     tags:
     *      - Templates
     *     description: Getting all templates
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         schema:
     *           type: object
     *
     * /templates/{template_name}:
     *   get:
     *     tags:
     *      - Templates
     *     description: Getting template by name
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: template_name
     *         in: path
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         schema:
     *           type: object
     */
    try {
      const result = await templateModel.findAll(req.params.template_name);
      res.send(result);
    } catch (e) {
      console.error(e);
      return res.send(500, { error: '' });
    }
  }
}

module.exports = { Template };

const { AuthMiddleware } = require ('./authMiddleware');
const { Mailer } = require ('../models/mailer');

const MailerModel = new Mailer ();
const Router = require ('restify-router').Router;
const routerInstance = new Router ();

class Mail {
  constructor (server) {
    this.registerRoutes ();
    return routerInstance.applyRoutes (server);
  }

  registerRoutes () {
    routerInstance.get ('/mail', AuthMiddleware.checkApplicationToken (),
      this.getAllMails.bind (this));
    routerInstance.post ('/mail', AuthMiddleware.checkApplicationToken (),
      this.sendMail.bind (this));
  }

  async getAllMails (req, res, next) {

    /**
     * @swagger
     * /mail:
     *   get:
     *     tags:
     *      - Mail
     *     description: Getting all sent emails
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         schema:
     *           type: array
     */
    try {
      const result = await MailerModel.findAll();
      res.send (result);
    } catch (e) {
      console.error (e);
      return res.send (500, { error: e.message });
    }
  }

  async sendMail (req, res, next) {

    /**
     * @swagger
     * /mail:
     *   post:
     *     tags:
     *      - Mail
     *     description: send a mail
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: body
     *         in: body
     *         type: object
     *         schema:
     *           $ref: '#/definitions/Mail'
     *     responses:
     *       200:
     *         schema:
     *           type: object
     */
    const { template, params, emails } = req.body;
    if (!template || !emails || !Array.isArray (emails) || Array.isArray (emails) &&
        !emails.length) {
      return res.send (400, { error: 'Not valid body' });
    }

    Object.assign (params, { emails });

    try {
      const result = await MailerModel.sendMail(template, params);
      res.send (result);
    } catch (e) {
      console.error (e);
      return res.send (500, { error: e.message });
    }
  }
}

module.exports = { Mail };

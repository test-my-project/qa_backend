const { AbstractDB } = require('./DB');

class ProjectsModel extends AbstractDB {
  constructor () {
    super();
    this.projects = this.sequelize.define('projects', {
      owner: this.DataTypes.INTEGER,
      company: this.DataTypes.INTEGER,
      name: this.DataTypes.STRING,
      work_type: this.DataTypes.ENUM('Site', 'Design', 'Prototypes'),
      url: this.DataTypes.STRING,
      date_start: this.DataTypes.DATE,
      date_end: this.DataTypes.DATE,
      planning_time: this.DataTypes.INTEGER,
      fact_time: this.DataTypes.INTEGER,
      avatar: this.DataTypes.STRING,
      status: this.DataTypes.BOOLEAN,
      type: this.DataTypes.ENUM('developing', 'supporting'),
      description: this.DataTypes.STRING,
      hex: this.DataTypes.STRING,
      createdAt: this.DataTypes.DATE,
      updatedAt: this.DataTypes.DATE,
    });
  }

  associate (projects_connectors, projects_members) {
    this.projects.hasMany(projects_connectors, { foreignKey: 'parent_project_id' });
    this.projects.hasMany(projects_members, { foreignKey: 'project_id' });
  }

  get Projects () {
    return this.projects;
  }
}

module.exports = { ProjectsModel };
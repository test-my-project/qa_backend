const { ProjectsModel } = require('./projects');
const { ProjectConnectorsModel } = require('./projects_connectors');
const { ProjectMembersModel } = require('./projects_members');

const projectsModel = new ProjectsModel();
const projectConnectorsModel = new ProjectConnectorsModel();
const projectMembersModel = new ProjectMembersModel();

projectsModel.associate(projectConnectorsModel.ProjectConnectors, projectMembersModel.ProjectMembers);
projectConnectorsModel.associate(projectsModel.Projects);
projectMembersModel.associate(projectsModel.Projects);


module.exports = {
  Projects: projectsModel.Projects,
  Childs: projectConnectorsModel.ProjectConnectors,
  Members: projectMembersModel.ProjectMembers,
};
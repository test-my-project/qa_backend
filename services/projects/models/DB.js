const Sequelize = require('sequelize');
const config = require('config');
process.setMaxListeners(0);

class AbstractDB {
  constructor () {
    this.sequelize = new Sequelize(
      config.get('db.database'),
      config.get('db.username'),
      config.get('db.password'),
      {
        host: config.get('db.host'),
        port: config.get('db.port'),
        dialect: config.get('db.dialect'),
        pool: {
          max: 5,
          min: 0,
          idle: 10000,
        },
      });
    this.DataTypes = Sequelize.DataTypes;
  }
}

module.exports = { AbstractDB };
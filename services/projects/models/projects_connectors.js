const { AbstractDB } = require('./DB');

class ProjectConnectorsModel extends AbstractDB {
  constructor () {
    super();
    this.projects_connectors = this.sequelize.define('projects_connectors', {
      parent_project_id: this.DataTypes.INTEGER,
      child_project_id: this.DataTypes.INTEGER,
    }, { timestamps: false });
  }

  associate (projects) {
    this.projects_connectors.belongsTo(projects, {
      foreignKey: 'child_project_id',
      targetKey: 'id',
    });
  }

  get ProjectConnectors () {
    return this.projects_connectors;
  }
}

module.exports = { ProjectConnectorsModel };
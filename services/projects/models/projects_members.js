const { AbstractDB } = require('./DB');

class ProjectMembersModel extends AbstractDB {
  constructor () {
    super();
    this.projects_members = this.sequelize.define('projects_members', {
      project_id: this.DataTypes.INTEGER,
      user_id: this.DataTypes.INTEGER,
    }, { timestamps: false });
  }

  associate (projects) {
    this.projects_members.belongsTo(projects, {
      foreignKey: 'project_id',
      targetKey: 'id',
    });
  }

  get ProjectMembers () {
    return this.projects_members;
  }
}

module.exports = { ProjectMembersModel };
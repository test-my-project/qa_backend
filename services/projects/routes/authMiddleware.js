const fs = require('fs');
const path = require('path');
const authChecking = require('qa-auth-checking');
const config = require('config');

const authservice = new authChecking({
  authServer: config.get('auth'),
  decodeJWTKey: fs.readFileSync(path.resolve('./keys/' + config.get('auth.keyPath')), 'utf8'),
});

class AuthMiddleware {
  static async checkApplicationToken (req, res, next) {
    try {
      await authservice.checkApplicationToken(req.headers['x-access-token']);
      return next();
    } catch (e) {
      return res.send(401, e);
    }
  }

  static decodeUserJWT (token) {
    return authservice.decodeUserJWT(token);
  }
}

module.exports = { AuthMiddleware };
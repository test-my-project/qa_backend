const { Router } = require('restify-router');

const ProjectController = require('../controllers/projectController');
const { AuthMiddleware } = require('./authMiddleware');

const router = new Router();

class Projects extends ProjectController {
  constructor (server) {
    super();
    this.registerRoutes();
    return router.applyRoutes(server);
  }

  registerRoutes () {
    router.get('/', AuthMiddleware.checkApplicationToken, this.wrap(this.list.bind(this)));
    router.get('/:projectId', AuthMiddleware.checkApplicationToken, this.wrap(this.one.bind(this)));
    router.post('/', AuthMiddleware.checkApplicationToken, this.wrap(this.add.bind(this)));
    router.put('/:projectId', AuthMiddleware.checkApplicationToken, this.wrap(this.update.bind(this)));
    router.del('/:projectId', AuthMiddleware.checkApplicationToken, this.wrap(this.delete.bind(this)));
  }

  wrap (handler) {
    return async (req, res, next) => {
      try {
        await handler(req, res, next);
      } catch (err) {
        return res.send(err.status || 500, { ...err });
      }
    };
  }

  /**
   * @swagger
   * /:
   *   get:
   *     tags:
   *      - Projects
   *     description: get projects
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: x-access-token
   *         description: Application access token.
   *         in: header
   *         required: true
   *         type: string
   *       - name: access-token
   *         description: User access token.
   *         in: header
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         schema:
   *           type: array
   *           items:
   *             $ref: '#/definitions/Project'
   */
  async list (req, res) {
    let memberId;
    try {
      const { payload: { id } } = await AuthMiddleware.decodeUserJWT(req.headers['access-token']);
      memberId = id;
    } catch (e) {
      throw new Error({
        ...e,
        status: 401,
      });
    }
    const list = await this.getProjectList(memberId);
    res.send(list);
  }

  /**
   * @swagger
   * /{projectId}:
   *   get:
   *     tags:
   *      - Projects
   *     description: get projects
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: projectId
   *         description: Application access token.
   *         in: path
   *         required: true
   *         type: string
   *       - name: x-access-token
   *         description: Application access token.
   *         in: header
   *         required: true
   *         type: string
   *       - name: access-token
   *         description: User access token.
   *         in: header
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         schema:
   *           type: array
   *           items:
   *             $ref: '#/definitions/Project'
   */
  async one (req, res) {
    let memberId;
    try {
      const { payload: { id } } = await AuthMiddleware.decodeUserJWT(req.headers['access-token']);
      memberId = id;
    } catch (e) {
      throw new Error({
        ...e,
        status: 401,
      });
    }
    const list = await this.getOneProject(req.params.projectId, memberId);
    res.send(list);
  }

  /**
   * @swagger
   * /:
   *   post:
   *     tags:
   *      - Projects
   *     description: create projects
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: x-access-token
   *         description: Application access token.
   *         in: header
   *         required: true
   *         type: string
   *       - name: access-token
   *         description: app name
   *         in: header
   *         required: true
   *         type: string
   *       - name: body
   *         in: body
   *         type: object
   *         schema:
   *           $ref: '#/definitions/Project'
   *     responses:
   *       200:
   *         schema:
   *           type: array
   *           items:
   *             $ref: '#/definitions/Project'
   */
  async add (req, res) {
    let projectData = req.body;
    try {
      const { payload: { id } } = await AuthMiddleware.decodeUserJWT(req.headers['access-token']);
      projectData.owner = id; // TODO: get real user ID
    } catch (e) {
      throw new Error({
        ...e,
        status: 401,
      });
    }
    const result = await this.addProject(projectData);
    res.send(result);
  }

  /**
   * @swagger
   * /{projectId}:
   *   put:
   *     tags:
   *      - Projects
   *     description: update projects
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: projectId
   *         description: Application access token.
   *         in: path
   *         required: true
   *         type: string
   *       - name: x-access-token
   *         description: Application access token.
   *         in: header
   *         required: true
   *         type: string
   *       - name: access-token
   *         description: app name
   *         in: header
   *         required: true
   *         type: string
   *       - name: body
   *         in: body
   *         type: object
   *         schema:
   *           $ref: '#/definitions/ProjectUpdate'
   *     responses:
   *       200:
   *         schema:
   *           type: array
   *           items:
   *             $ref: '#/definitions/Project'
   */
  async update (req, res) {
    let memberId;
    try {
      const { payload: { id } } = await AuthMiddleware.decodeUserJWT(req.headers['access-token']);
      memberId = id;
    } catch (e) {
      throw new Error({
        ...e,
        status: 401,
      });
    }
    const result = await this.updateProject(req.params.projectId, memberId, req.body);
    res.send(result);
  }

  /**
   * @swagger
   * /{projectId}:
   *   delete:
   *     tags:
   *      - Projects
   *     description: update projects
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: projectId
   *         description: Application access token.
   *         in: path
   *         required: true
   *         type: string
   *       - name: x-access-token
   *         description: Application access token.
   *         in: header
   *         required: true
   *         type: string
   *       - name: access-token
   *         description: app name
   *         in: header
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         schema:
   *           type: array
   *           items:
   *             $ref: '#/definitions/Project'
   */
  async delete (req, res) {
    let memberId;
    try {
      const { payload: { id } } = await AuthMiddleware.decodeUserJWT(req.headers['access-token']);
      memberId = id;
    } catch (e) {
      throw new Error({
        ...e,
        status: 401,
      });
    }
    const deletedCount = await this.deleteProject(req.params.projectId, memberId);
    res.send({ deletedCount });
  }
}

module.exports = { Projects };

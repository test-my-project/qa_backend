const { Router } = require('restify-router');

const { MembersController } = require('../controllers/membersController');
const { AuthMiddleware } = require('./authMiddleware');

const router = new Router();

class Members extends MembersController {
  constructor (server) {
    super();
    this.registerRoutes();
    return router.applyRoutes(server);
  }

  registerRoutes () {
    router.get('/:projectId/member', AuthMiddleware.checkApplicationToken, this.wrap(this.list.bind(this)));
    router.post('/:projectId/member', AuthMiddleware.checkApplicationToken, this.wrap(this.add.bind(this)));
    router.del('/:projectId/member', AuthMiddleware.checkApplicationToken, this.wrap(this.delete.bind(this)));
    router.del('/:projectId/member/:memberId', AuthMiddleware.checkApplicationToken, this.wrap(this.delete.bind(this)));
  }

  wrap (handler) {
    return async (req, res, next) => {
      try {
        await handler(req, res, next);
      } catch (err) {
        return res.send(err.status || 500, { ...err });
      }
    };
  }

  /**
   * @swagger
   * /{projectId}/member:
   *   get:
   *     tags:
   *      - Members
   *     description: get members assigned to project by project id
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: projectId
   *         description: Application access token.
   *         in: path
   *         required: true
   *         type: string
   *       - name: x-access-token
   *         description: Application access token.
   *         in: header
   *         required: true
   *         type: string
   *       - name: access-token
   *         description: user jwt.
   *         in: header
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         schema:
   *           type: array
   *           items:
   *             $ref: '#/definitions/project_members'
   */
  async list (req, res) {
    const list = await this.getMembersList(req.params.projectId);
    res.send(list);
  }

  /**
   * @swagger
   * /{projectId}/member:
   *   post:
   *     tags:
   *      - Members
   *     description: assign user to project as member
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: projectId
   *         description: Application access token.
   *         in: path
   *         required: true
   *         type: string
   *       - name: x-access-token
   *         description: Application access token.
   *         in: header
   *         required: true
   *         type: string
   *       - name: access-token
   *         description: user jwt
   *         in: header
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         schema:
   *           type: array
   *           items:
   *             $ref: '#/definitions/project_members'
   */
  async add (req, res) {
    let memberId;
    try {
      const { payload: { id } } = await AuthMiddleware.decodeUserJWT(req.headers['access-token']);
      memberId = id;
    } catch (e) {
      throw new Error({
        ...e,
        status: 401,
      });
    }
    const result = await this.addMemberToProject(req.params.projectId, memberId);
    res.send(result);
  }

  /**
   * @swagger
   * /{projectId}/member:
   *   delete:
   *     tags:
   *      - Members
   *     description: remove user from project
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: projectId
   *         description: Project ID.
   *         in: path
   *         required: true
   *         type: string
   *       - name: x-access-token
   *         description: Application access token.
   *         in: header
   *         required: true
   *         type: string
   *       - name: access-token
   *         description: user jwt
   *         in: header
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         schema:
   *           type: Object
   *           parameters:
   *            - name: deletedCount
   *              type: number
   * /{projectId}/member/{memberId}:
   *   delete:
   *     tags:
   *      - Members
   *     description: remove user from project
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: projectId
   *         description: Project ID.
   *         in: path
   *         required: true
   *         type: string
   *       - name: memberId
   *         description: Project ID.
   *         in: path
   *         required: true
   *         type: string
   *       - name: x-access-token
   *         description: Application access token.
   *         in: header
   *         required: true
   *         type: string
   *       - name: access-token
   *         description: user jwt
   *         in: header
   *         required: true
   *         type: string
   *     responses:
   *       200:
   *         schema:
   *           type: Object
   *           parameters:
   *            - name: deletedCount
   *              type: number
   */
  async delete (req, res) {
    let memberId;
    try {
      const { payload: { id } } = await AuthMiddleware.decodeUserJWT(req.headers['access-token']);
      memberId = id;
    } catch (e) {
      throw new Error({
        ...e,
        status: 401,
      });
    }
    const deletedCount = await this.deleteMemberFromProject(req.params.projectId,
      req.params.memberId || memberId);
    res.send({ deletedCount });
  }
}

module.exports = { Members };

# projects

## Scripts

`yarn start` - start projects service
`yarn migrations-up` - run migrations
`yarn migrations-down` - rollback last migration
`yarn migrations-create test` - creates a new migration with name `test`

## Api

* [projects swagger](https://projects.qa-services.gq/swagger/)



const restify = require('restify');
const plugins = require('restify-plugins');
const swaggerJSDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-restify');
const CookieParser = require('restify-cookies');
const config = require('config');

const server = restify.createServer({
  name: 'qa-projects',
  version: '0.0.1',
});

server.pre(restify.pre.sanitizePath());
server.use(plugins.acceptParser(server.acceptable));
server.use(plugins.queryParser());
server.use(plugins.bodyParser());
server.use(CookieParser.parse);

const { Projects } = require('./routes/projects');
const { Members } = require('./routes/members');

if (process.env.NODE_ENV === 'localhost') {
  server.pre(restify.CORS());
  server.pre(restify.fullResponse());

  let unknownMethodHandler = function (req, res) {
    if (req.method.toLowerCase() === 'options') {
      const allowHeaders = [
        'Accept',
        'Accept-Version',
        'Content-Type',
        'Api-Version',
        'Origin',
        'X-Requested-With',
        'Authorization']; // added Origin & X-Requested-With & **Authorization**

      if (res.methods.indexOf('OPTIONS') === -1) res.methods.push('OPTIONS');

      res.header('Access-Control-Allow-Credentials', true);
      res.header('Access-Control-Allow-Headers', allowHeaders.join(', '));
      res.header('Access-Control-Allow-Methods', res.methods.join(', '));
      res.header('Access-Control-Allow-Origin', req.headers.origin);

      return res.send(200);
    } else {
      return res.send(new restify.MethodNotAllowedError());
    }
  };

  server.on('MethodNotAllowed', unknownMethodHandler);
}

const options = {
  swaggerDefinition: {
    info: {
      title: 'API documentation for projects service', // Page title (required)
      version: '0.0.1', // Server version (required)
    },
  },
  apis: [
    `${__dirname}/routes/*.js`,
    `${__dirname}/server.js`,
  ],
  tags: [
    {
      name: 'Projects',
    },
    {
      name: 'Members',
    },
  ],
};

const swaggerSpec = swaggerJSDoc(options);

server.get('/swagger/swagger.json', function (req, res) {
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

server.get(/\/swagger-ui+.*/, ...swaggerUi.serveFiles(swaggerSpec));
server.get('/swagger', swaggerUi.setup(swaggerSpec));

new Projects(server);
new Members(server);

server.get(/.*/, restify.serveStatic({
  directory: __dirname + '/coverage/lcov-report',
  default: 'index.html',
}));

server.listen(process.env.port || config.get('serverPort') || 3090, function () {
  console.log('%s listening at %s', server.name, server.url);
});

module.exports = server;


/**
 * @swagger
 * definitions:
 *
 *   Child:
 *     type: object
 *     properties:
 *       name:
 *         type: string
 *       work_type:
 *         type: string
 *         enum: [Site, Design, Prototypes]
 *       url:
 *         type: string
 *       date_start:
 *         type: string
 *       date_end:
 *         type: string
 *       planing_time:
 *         type: integer
 *       fact_time:
 *         type: integer
 *       avatar:
 *         type: string
 *       status:
 *         type: boolean
 *       type:
 *         type: string
 *         enum: [developing, supporting]
 *       description:
 *         type: string
 *       hex:
 *         type: string
 *
 *   Project:
 *     type: object
 *     properties:
 *       company:
 *         type: integer
 *       name:
 *         type: string
 *       work_type:
 *         type: string
 *         enum: [Site, Design, Prototypes]
 *       url:
 *         type: string
 *       date_start:
 *         type: string
 *       date_end:
 *         type: string
 *       planing_time:
 *         type: integer
 *       fact_time:
 *         type: integer
 *       avatar:
 *         type: string
 *       status:
 *         type: boolean
 *       type:
 *         type: string
 *         enum: [developing, supporting]
 *       description:
 *         type: string
 *       hex:
 *         type: string
 *       childs:
 *         type: array
 *         items:
 *           $ref: '#/definitions/Child'
 *
 *   project_members:
 *     type: object
 *     properties:
 *       id:
 *         type: integer
 *       project_id:
 *         type: integer
 *       user_id:
 *         type: integer
 *
 *   ProjectUpdate:
 *     type: object
 *     properties:
 *       company:
 *         type: integer
 *       name:
 *         type: string
 *       work_type:
 *         type: string
 *         enum: [Site, Design, Prototypes]
 *       url:
 *         type: string
 *       date_start:
 *         type: string
 *       date_end:
 *         type: string
 *       planing_time:
 *         type: integer
 *       fact_time:
 *         type: integer
 *       avatar:
 *         type: string
 *       status:
 *         type: boolean
 *       type:
 *         type: string
 *         enum: [developing, supporting]
 *       description:
 *         type: string
 *       hex:
 *         type: string
 */

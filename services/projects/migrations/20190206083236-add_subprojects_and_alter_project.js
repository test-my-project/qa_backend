'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    const promises = [
      queryInterface.changeColumn('projects', 'avatar', Sequelize.TEXT),
      queryInterface.addColumn('projects', 'hex', Sequelize.TEXT),
      queryInterface.createTable('projects_connectors', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        parent_project_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'projects',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },
        child_project_id: {
          type: Sequelize.INTEGER,
          references: {
            model: 'projects',
            key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade'
        },
      })
    ]
    return Promise.all(promises)
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    const promises = [
      queryInterface.changeColumn('projects', 'avatar', Sequelize.STRING),
      queryInterface.removeColumn('projects', 'hex'),
      queryInterface.dropTable('projects_connector')
    ]
    return Promise.all(promises)
  }
};

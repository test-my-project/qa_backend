'use strict'

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('projects', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      owner: Sequelize.INTEGER,
      company: Sequelize.INTEGER,
      name: Sequelize.STRING,
      work_type: Sequelize.ENUM('Site', 'Design', 'Prototypes'),
      url: Sequelize.STRING,
      date_start: Sequelize.DATE,
      date_end: Sequelize.DATE,
      planning_time: Sequelize.INTEGER,
      fact_time: Sequelize.INTEGER,
      avatar: Sequelize.STRING,
      status: Sequelize.BOOLEAN,
      type: Sequelize.ENUM('developing', 'supporting'),
      description: Sequelize.STRING,
      createdAt: {
        type: Sequelize.DATE,
      },
      updatedAt: {
        type: Sequelize.DATE,
      },

    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('projects')
  },
}

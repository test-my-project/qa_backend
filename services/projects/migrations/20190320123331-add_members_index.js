'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return queryInterface.addIndex('projects_members', ['user_id'],{ concurrently: true })
      .then(() => queryInterface.addConstraint( 'projects_members', ['project_id', 'user_id'], { type: 'UNIQUE', name: 'project_id_user_id_constraint' }))
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    return queryInterface.removeIndex('projects_members', 'user_id').then(()=>
      queryInterface.removeConstraint('projects_members', 'project_id_user_id_constraint'))
  }
};

const request = require('request-promise');
const config = require('config');

const { Members } = require('../models');

const authUrl = `${config.get('auth.protocol')}://${config.get('auth.host')}:${config.get('auth.port')}`;

class MembersController {
  getMembersList (projectId) {
    return Members.findAll({
      where: { project_id: projectId },
    });
  }

  async addMemberToProject (project_id, user_id) {
    const parent = await Members.create({ project_id, user_id }, { returning: true });
    return parent.dataValues;
  }

  deleteMemberFromProject (project_id, user_id) {
    return Members.destroy({ where: { project_id, user_id } });
  }
}

function getInfo (user_id) {
  return request.get(`${authUrl}/users/${user_id}`, {
    json: true, headers: {
      'x-access-token': config.get('auth.hash'),
    },
  });
}

module.exports = { MembersController, getInfo };
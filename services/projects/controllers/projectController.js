const { getInfo: getUserInfo } = require('./membersController');

const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const {
  Projects,
  Childs,
  Members,
} = require('../models');

class ProjectController {
  populateMembers (project) {
    return Promise.all(project.dataValues.projects_members.map(async v => {
      const [user] = await getUserInfo(v.user_id);
      return user;
    }));
  }

  async getProjectList (user_id) {
    try {
      const projects = await Projects.findAll({
        where: {
          [Op.or]: [{ owner: user_id }, { '$projects_members.user_id$': user_id }],
          id: {
            [Op.notIn]: Sequelize.literal('(select child_project_id from projects_connectors)'),
          },
        },
        include: [
          {
            model: Childs,
            include: {
              model: Projects,
              include: { model: Members },
            },
          },
          {
            model: Members,
          },
        ],
      });

      return Promise.all(projects.map(async p => {
        const members = await this.populateMembers(p);
        const childs = await Promise.all(p.dataValues.projects_connectors.map(async v => {
          const members = await this.populateMembers(v.dataValues.project);
          return { ...v.dataValues.project.dataValues, members, projects_members: undefined };
        }));
        return {
          ...p.dataValues,
          childs,
          members,
          projects_connectors: undefined,
          projects_members: undefined,
        };
      }));
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  async getOneProject (id) {
    const projects = await Projects.findAll({
      where: { id },
      include: [
        {
          model: Childs,
          include: {
            model: Projects,
            include: { model: Members },
          },
        },
        {
          model: Members,
        },
      ],
    });
    return Promise.all(projects.map(async p => {
      const members = await this.populateMembers(p);
      const childs = await Promise.all(p.dataValues.projects_connectors.map(async v => {
        const members = await this.populateMembers(v.dataValues.project);
        return { ...v.dataValues.project.dataValues, members, projects_members: undefined };
      }));
      return {
        ...p.dataValues,
        childs,
        members,
        projects_connectors: undefined,
        projects_members: undefined,
      };
    }));
  }

  async addProject (projectData) {
    const parent = await Projects.create(projectData, { returning: true });
    if (projectData.childs) {
      Object.assign(projectData, {
        childs: projectData.childs.map(v => ({
          ...v,
          company: projectData.company,
          owner: projectData.owner,
        })),
      });
      const childs = await Projects.bulkCreate(projectData.childs, { returning: true });
      const dataForConnect = childs.map(v => ({
        parent_project_id: parent.get('id'),
        child_project_id: v.get('id'),
      }));
      await Childs.bulkCreate(dataForConnect);
      parent.dataValues.childs = childs;
    }
    return parent.dataValues;
  }

  updateProject (id, owner, data) {
    return Projects.update(data, { where: { id, owner } }, { returning: true });
  }

  async deleteProject (id, owner) {
    const subProject = await Childs.findAll({
      where: { parent_project_id: id },
      attributes: ['child_project_id'],
    });
    return Projects.destroy({
      where: {
        id: { [Op.in]: [id, ...subProjects.map(v => v.dataValues.child_project_id)] },
        owner,
      }, cascade: true,
    });
  }
}

module.exports = ProjectController;

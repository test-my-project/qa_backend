# auth

## Scripts

`yarn start` - start auth service
`yarn migrations-up` - run migrations
`yarn migrations-down` - rollback last migration
`yarn migrations-create test` - creates a new migration with name `test`

## Api

* [Auth & Users swagger](https://auth.qa-services.gq/swagger)



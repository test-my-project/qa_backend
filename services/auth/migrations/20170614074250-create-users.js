'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      email: {
        allowNull: false,
        type: Sequelize.STRING
      },
      display_name: {
        type: Sequelize.STRING
      },
      fb_id: {
        type: Sequelize.STRING
      },
      google_id: {
        type: Sequelize.STRING
      },
      linkedin_id: {
        type: Sequelize.STRING
      },
      password: {
        type: Sequelize.STRING
      },
      created_at: {
        type: Sequelize.DATE,
        defaultValue:Sequelize.literal('now()'),
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        defaultValue:Sequelize.literal('now()'),
        allowNull: false
      }
    }, {
      uniqueKeys: [
        {
          name: 'users_unique',
          singleField: false,
          fields: ['email']
        },
        {
          name: 'users_unique_fb',
          singleField: false,
          fields: ['fb_id']
        },
        {
          name: 'users_unique_google',
          singleField: false,
          fields: ['google_id']
        },
        {
          name: 'users_unique_linkedin',
          singleField: false,
          fields: ['linkedin_id']
        },
      ]
    })
  },

  down: function (queryInterface, Sequelize) {
    return queryInterface.dropTable('users');
  }
};

const { Router } = require('restify-router');

const { AuthMiddleware: Auth } = require('./authMiddleware');
const { UsersModel } = require('../models/users');

const { Users } = new UsersModel();
const routerInstance = new Router();
const AuthMiddleware = new Auth();

class User {
  constructor (server) {
    this.registerRoutes();
    return routerInstance.applyRoutes(server);
  }

  registerRoutes () {
    routerInstance.get(
      '/users',
      AuthMiddleware.checkApplicationToken(),
      this.getAllUsers.bind(this),
    );
    routerInstance.get(
      '/users/:user_id',
      AuthMiddleware.checkApplicationToken(),
      this.getAllUsers.bind(this),
    );
    routerInstance.put(
      '/users/:user_id',
      AuthMiddleware.checkApplicationToken(),
      this.updateUserInfo.bind(this),
    );
    routerInstance.get('/user/info', this.getUser.bind(this));
  }

  async getAllUsers (req, res, next) {

    /**
     * @swagger
     * /users:
     *   get:
     *     tags:
     *      - User
     *     description: Getting all users
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/UserSubscribe'
     * /users/{user_id}:
     *   get:
     *     tags:
     *      - User
     *     description: Getting all users
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: user_id
     *         description: User id.
     *         in: path
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/UserSubscribe'
     */
    let query = {};
    console.log(req.params);
    if (req.params.user_id && !!req.params.user_id.trim() === true) {
      query = { where: { id: req.params.user_id } };
    }
    try {
      console.log(query);
      const result = await Users.findAll(query);
      res.send(result);
      next();
    } catch (e) {
      return res.send(500, { error: '' });
    }
  }

  async getUser (req, res, next) {

    /**
     * @swagger
     * /user/info:
     *   get:
     *     tags:
     *      - User
     *     description: Getting info by user access_token
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: access-token
     *         description: User access token.
     *         in: header
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/UserSubscribe'
     */
    if (!req.headers['access-token']) {
      return res.send(400, { message: 'Access token empty' });
    }
    try {
      const user = await AuthMiddleware.checkUserToken(req.headers['access-token']);
      res.send(user);
      next();
    } catch (e) {
      return res.send(e.statusCode, e);
    }
  }

  async updateUserInfo (req, res, next) {

    /**
     * @swagger
     * /users/{user_id}:
     *   put:
     *     tags:
     *      - User
     *     description: Getting info by user access_token
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: user_id
     *         description: User id.
     *         in: path
     *         required: true
     *         type: string
     *       - name: body
     *         in: body
     *         type: object
     *         schema:
     *           type: object
     *           properties:
     *             email:
     *               type: string
     *             display_name:
     *               type: string
     *             password:
     *               type: string
     *     responses:
     *       200:
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/UserSubscribe'
     */
    let { user_id } = req.params,
      { body } = req;
    if (!user_id) {
      return res.send(400, {
        message: 'User id empty',
      });
    }
    try {
      const rows = await Users.update(body, { where: { id: user_id } });
      res.send({ rewrite: rows });
      next();
    } catch (e) {
      return res.send(e.statusCode, e);
    }
  }

}

module.exports = { User };

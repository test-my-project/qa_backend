'use strict';
const Client = require('jscent');
const path = require('path');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const config = require('config');

const { ApplicationsModel } = require('../models/applications');

const publicKey = fs.readFileSync(path.join(__dirname, '..', 'keys', 'back.pub'), 'utf8');
const privateKey = fs.readFileSync(path.join(__dirname, '..', 'keys', 'back'), 'utf8');

const TokenGen = new Client.Token(config.get('secretKey'));

class AuthMiddleware extends ApplicationsModel {
  constructor () {
    super();
  }

  checkApplicationToken () {
    return (req, res, next) => {
      this.checkKey(req.headers['x-access-token']).then((checked) => {
        req.application = {
          ...checked,
          pubKey: fs.readFileSync(path.join(__dirname, '..', 'keys', checked.name + '.pub')),
        };
        return next();
      }).catch((e) => {
        return res.send(401, e);
      });
    };
  }

  async createUserToken (user) {
    const u = user.email;
    const timestamp = Math.floor(Date.now() / 1000).toString();
    user.token = TokenGen.clientToken(u, timestamp, '');

    let token = jwt.sign(user, privateKey, { algorithm: 'RS256' });
    return { token };
  }

  async checkUserToken (token) {
    try {
      jwt.verify(token, publicKey, { algorithm: 'RS256' });
      return jwt.decode(token, { complete: true });
    } catch (err) {
      console.log(err);
      throw err;
    }
  }
}

module.exports = { AuthMiddleware };
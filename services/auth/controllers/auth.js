const crypto = require('crypto');
const config = require('config');
const { Router } = require('restify-router');

const { AuthMiddleware: Auth } = require('./authMiddleware');
const { UsersModel } = require('../models/users');

const { Users } = new UsersModel();
const routerInstance = new Router();
const AuthMiddleware = new Auth();

class AuthController {
  constructor (server) {
    this.registerRoutes();
    return routerInstance.applyRoutes(server);
  }

  registerRoutes () {
    routerInstance.post('/sign_up', AuthMiddleware.checkApplicationToken(), this.signUp.bind(this));
    routerInstance.post('/login', AuthMiddleware.checkApplicationToken(), this.login.bind(this));
  }

  async signUp (req, res, next) {

    /**
     * @swagger
     * /sign_up:
     *   post:
     *     tags:
     *      - Auth
     *     description: Register new user
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: body
     *         description: User access token.
     *         in: body
     *         schema:
     *           type: object
     *           properties:
     *             email:
     *               type: string
     *             display_name:
     *               type: string
     *             password:
     *               type: string
     *
     *     responses:
     *       200:
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/user'
     */
    if (!req.headers['x-access-token']) {
      return res.send(400, { message: 'Application access token empty' });
    }
    let { password, display_name, email } = req.body;

    if (!!password == false || !!email == false) {
      return res.send(400, { message: 'Not valid request format' });
    }

    const hmac = crypto.createHmac('sha256', config.get('secretKey'));
    password = hmac.update(password).digest('hex');
    email = email.toLowerCase();
    try {
      const result = await Users.create({ password, display_name, email });
      res.send(await AuthMiddleware.createUserToken(result.dataValues));
    } catch (e) {
      return res.send(500, e.errors[0]);
    }

  }

  async login (req, res, next) {

    /**
     * @swagger
     * /login:
     *   post:
     *     tags:
     *      - Auth
     *     description: Login new user
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: body
     *         description: User access token.
     *         in: body
     *         schema:
     *           type: object
     *           properties:
     *             email:
     *               type: string
     *             password:
     *               type: string
     *
     *     responses:
     *       200:
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/user'
     */
    if (!req.headers['x-access-token']) {
      return res.send(400, { message: 'Application access token empty' });
    }
    let { password, email } = req.body;

    if (!!password == false || !!email == false) {
      return res.send(400, { message: 'Not valid request format' });
    }

    const hmac = crypto.createHmac('sha256', config.get('secretKey'));

    password = hmac.update(password).digest('hex');

    let query = {
      password,
      email,
    };

    if ('email' in query) {
      Object.assign(query, {
        $col: Users.sequelize.where(Users.sequelize.fn('lower', Users.sequelize.col('email')),
          Users.sequelize.fn('lower', query.email)),
      });
      delete query.email;
    }
    try {
      const result = await Users.findOne({
        where: query,
      });
      if (!result) {
        return res.send(404, { message: 'User not found' });
      }

      return res.send(await AuthMiddleware.createUserToken(result.dataValues));
    } catch (e) {
      return res.send(500, e);
    }
  }

}

module.exports = { AuthController };

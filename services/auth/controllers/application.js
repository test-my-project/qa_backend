const Router = require('restify-router').Router;

const { AuthMiddleware: Auth } = require('./authMiddleware');

const AuthMiddleware = new Auth();
const routerInstance = new Router();

class Applications {
  constructor (server) {
    this.registerRoutes();
    return routerInstance.applyRoutes(server);
  }

  registerRoutes () {
    routerInstance.post(
      '/application',
      AuthMiddleware.checkApplicationToken(),
      this.createKeyPair.bind(this),
    );
    routerInstance.get(
      '/application',
      AuthMiddleware.checkApplicationToken(),
      this.chechAppToken.bind(this),
    );
  }

  async createKeyPair (req, res, next) {

    /**
     * @swagger
     * /application:
     *   post:
     *     tags:
     *      - Apps
     *     description: Creating keys pair for app
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     *       - name: name
     *         description: app name
     *         in: query
     *         required: true
     *         type: string
     *     responses:
     *       200:
     *         schema:
     *           type: array
     *           items:
     *             $ref: '#/definitions/UserSubscribe'
     */
    const publickKey = AuthMiddleware.createKeyPair(req.query.name);
    res.send({ key: publickKey });
    next();
  }

  async chechAppToken (req, res, next) {

    /**
     * @swagger
     * /application:
     *   get:
     *     tags:
     *      - Apps
     *     description: Creating keys pair for app
     *     produces:
     *       - application/json
     *     parameters:
     *       - name: x-access-token
     *         description: Application access token.
     *         in: header
     *         required: true
     *         type: string
     */
    res.send();
    next();
  }

}

module.exports = { Applications };
const config = require('config');
const path = require('path');
const fs = require('fs');
const NodeRSA = require('node-rsa');

let keysArray = [];

const keys = function () {
  fs.readdir(path.join(__dirname, '..', 'keys'), (err, files) => {
    keysArray = files.filter(v => v.indexOf('.pub') == -1).map(name => {
      let key = new NodeRSA();
      key.importKey(fs.readFileSync(path.join(__dirname, '..', 'keys', name)));
      return { name, key };
    });
    keysArray.forEach(v => {
      'use strict';
      console.log(v.name, v.key.sign(config.get('secretKey'), 'base64', 'utf8'));
    });
  });
};
keys();

class ApplicationsModel {
  async checkKey (hash) {
    let key = keysArray.find((v) => {
      return v.key.verify(config.get('secretKey'), hash, 'utf8', 'base64');
    });
    if (key) {
      return key;
    } else {
      throw {
        message: 'Key not valid',
      };
    }
  }

  createKeyPair (name) {
    let _key = new NodeRSA();
    _key.generateKeyPair();
    let pr = _key.exportKey('private');
    let pb = _key.exportKey('public');
    fs.writeFileSync(path.join(__dirname, '..', 'keys', name), pr);
    fs.writeFileSync(path.join(__dirname, '..', 'keys', name + '.pub'), pb);
    keys();
    return _key.sign(config.get('secretKey'), 'base64', 'utf8');
  }
}

module.exports = { ApplicationsModel };
const { AbstractDB } = require('./DB');

class UsersModel extends AbstractDB {
  constructor () {
    super();
    this.users = this.sequelize.define('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: this.DataTypes.INTEGER,
      },
      email: {
        allowNull: false,
        type: this.DataTypes.STRING,
      },
      display_name: {
        type: this.DataTypes.STRING,
      },
      fb_id: {
        type: this.DataTypes.STRING,
      },
      google_id: {
        type: this.DataTypes.STRING,
      },
      linkedin_id: {
        type: this.DataTypes.STRING,
      },
      password: {
        type: this.DataTypes.STRING,
      },
    }, {
      underscored: true,
      tableName: 'users',
    });
  }

  get Users () {
    return this.users;
  }
}

module.exports = { UsersModel };
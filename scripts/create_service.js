const fs = require('fs');
const path = require('path');

const [, , ...args] = process.argv;

const [serviceName] = args;

if (!serviceName) {
  throw new Error('Service name is required as first argument');
}

const newServicePath = path.resolve(__dirname, '..', 'services', serviceName);

let exist = true;

try {
  fs.statSync(newServicePath);
} catch (e) {
  exist = false;
}

if (exist) {
  throw new Error(`Service with name - ${ serviceName } already exists`);
}

function copyFileSync (source, target) {

  let targetFile = target;

  //if target is a directory a new file with the same name will be created
  if (fs.existsSync(target)) {
    if (fs.lstatSync(target).isDirectory()) {
      targetFile = path.join(target, path.basename(source));
    }
  }

  fs.writeFileSync(targetFile, fs.readFileSync(source));
}

function copyFolderRecursiveSync (source, target) {
  let files = [];

  //check if folder needs to be created or integrated
  const targetFolder = path.join(target, path.basename(source));
  if (!fs.existsSync(targetFolder)) {
    fs.mkdirSync(targetFolder);
  }

  //copy
  if (fs.lstatSync(source).isDirectory()) {
    files = fs.readdirSync(source);
    files.forEach(function(file) {
      const curSource = path.join(source, file);
      if (fs.lstatSync(curSource).isDirectory()) {
        copyFolderRecursiveSync(curSource, targetFolder);
      } else {
        copyFileSync(curSource, targetFolder);
      }
    });
  }
}

function replaceDefaultServiceName (fileName) {
  const file = fs.readFileSync(path.join(newServicePath, fileName), 'utf8');
  const replaced = file.replace(/\$SERVICE_NAME/gim, serviceName);
  return fs.writeFileSync(path.join(newServicePath, fileName), replaced);
}

copyFolderRecursiveSync(path.resolve(__dirname, 'service_template'), path.resolve(__dirname, '..', 'services'));
fs.renameSync(path.resolve(__dirname, '..', 'services', 'service_template'), newServicePath);

replaceDefaultServiceName('Dockerfile');
replaceDefaultServiceName('docker-compose.yml');
replaceDefaultServiceName('server.js');
replaceDefaultServiceName('package.json');
replaceDefaultServiceName('README.md');
const ci = Buffer.from(`
${serviceName} build:
  <<: *build
  stage: build
  variables:
    SERVICE: ${serviceName}
  only:
    refs:
      - master
    changes:
      - services/${serviceName}/**/*

${serviceName} Deploy:
  <<: *deploy
  stage: deploy
  only:
    refs:
      - master
    changes:
      - services/${serviceName}/**/*
  environment: production
  variables:
    NODE_ENV: "production"
    SERVICE: ${serviceName}

`);
fs.writeFileSync(path.resolve(__dirname, '..', '.gitlab-ci.yml'), ci, {
  flag: 'as'
});

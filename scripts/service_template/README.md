# qa_$SERVICE_NAME

## Scripts

`yarn start` - start $SERVICE_NAME service
`yarn migrations-up` - run migrations
`yarn migrations-down` - rollback last migration
`yarn migrations-create test` - creates a new migration with name `test`

## Api

* [$SERVICE_NAME swagger](https://$SERVICE_NAME.qa-services.gq/swagger)


